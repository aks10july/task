<?php

namespace App\Http\Controllers\webservices;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class webservicesController extends Controller {

    public function saveCompanyDetails(Request $request, $debug = 0) {

        if ($debug) {
//Testing values
            $param = [
                'name' => 'Aksahy Group',
                'email' => 'akshay@gmail.com',
                'logo' => 'logo.jpg',
            ];
            $request->request->add($param);
        }

        try {
            $messages = [
                'name.required' => __('User Name not found!'),
                'email.required' => __('Email not found!'),
            ];

            $rules = [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'logo' => 'mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=100,min_height=100',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                $returnArray = [
                    'status' => false,
                    'message' => $error,
                ];
            } else {

                $data = [
                    'name' => $request->name,
                    'email' => $request->email,
                ];
                if ($request->hasFile('logo')) {
                    $data['logo'] = $request->file('logo')->storePublicly('company/', 'public');
                }
                $model = new \App\Model\Admin\Company();
                if ($model = $model->create($data)) {

                    $returnArray = [
                        'status' => true,
                        'message' => 'Company Details save Successfully.',
                        'data' => $model->toArray(),
                    ];
                } else {
                    $returnArray = [
                        'status' => false,
                        'message' => 'Something went wrong',
                        'data' => [],
                    ];
                }
            }
        } catch (Exception $ex) {
            $returnArray = [
                'status' => false,
                'message' => $ex->getMessage(),
            ];
        }

        return response()->json($returnArray);
    }

    public function updateCompanyDetails(Request $request, $debug = 0) {

        if ($debug) {
//Testing values
            $param = [
                'name' => 'Aksahy Group',
                'email' => 'akshay@gmail.com',
                'logo' => 'logo.jpg',
                'id' => 1,
            ];
            $request->request->add($param);
        }

        try {
            $messages = [
                'name.required' => __('User Name not found!'),
                'email.required' => __('Email not found!'),
            ];

            $rules = [
                'id' => 'required|exists:companies',
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'logo' => 'mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=100,min_height=100',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                $returnArray = [
                    'status' => false,
                    'message' => $error,
                ];
            } else {

                $data = [
                    'name' => $request->name,
                    'email' => $request->email,
                ];

                $model = \App\Model\Admin\Company::find($request->id);

                if ($request->hasFile('logo')) {
                    Storage::delete('public/' . $model->logo);
                    $data['logo'] = $request->file('logo')->storePublicly('company/', 'public');
                }

                if ($model = $model->update($data)) {

                    $returnArray = [
                        'status' => true,
                        'message' => 'Company Details updated Successfully.',
                        'data' => $model->toArray(),
                    ];
                } else {
                    $returnArray = [
                        'status' => false,
                        'message' => 'Something went wrong',
                        'data' => [],
                    ];
                }
            }
        } catch (Exception $ex) {
            $returnArray = [
                'status' => false,
                'message' => $ex->getMessage(),
            ];
        }

        return response()->json($returnArray);
    }

    public function getCompanyDetailsById(Request $request, $debug = 0) {

        if ($debug) {
//Testing values
            $param = [
                'id' => 1
            ];
            $request->request->add($param);
        }

        try {
            $messages = [
                'id.required' => __('Id not found!'),
            ];

            $rules = [
                'id' => 'required|exists:companies',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                $returnArray = [
                    'status' => false,
                    'message' => $error,
                ];
            } else {

                $model = \App\Model\Admin\Company::find($request->id);

                if ($model) {

                    $returnArray = [
                        'status' => true,
                        'message' => 'Company Details found Successfully.',
                        'data' => $model->toArray(),
                    ];
                } else {
                    $returnArray = [
                        'status' => false,
                        'message' => 'Something went wrong',
                        'data' => [],
                    ];
                }
            }
        } catch (Exception $ex) {
            $returnArray = [
                'status' => false,
                'message' => $ex->getMessage(),
            ];
        }

        return response()->json($returnArray);
    }

    public function deleteCompanyById(Request $request, $debug = 0) {

        if ($debug) {
//Testing values
            $param = [
                'id' => 1
            ];
            $request->request->add($param);
        }

        try {
            $messages = [
                'id.required' => __('Id not found!'),
            ];

            $rules = [
                'id' => 'required|exists:companies',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            if ($validator->fails()) {
                $error = $validator->errors()->first();
                $returnArray = [
                    'status' => false,
                    'message' => $error,
                ];
            } else {

                $model = \App\Model\Admin\Company::find($request->id);

                if ($model) {
                    Storage::delete('public/' . $model->logo);
                    $model->delete();
                    $returnArray = [
                        'status' => true,
                        'message' => 'Company deleted Successfully.',
                        'data' => $model->toArray(),
                    ];
                } else {
                    $returnArray = [
                        'status' => false,
                        'message' => 'Something went wrong',
                        'data' => [],
                    ];
                }
            }
        } catch (Exception $ex) {
            $returnArray = [
                'status' => false,
                'message' => $ex->getMessage(),
            ];
        }

        return response()->json($returnArray);
    }

}
