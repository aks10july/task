<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller {

    /**
     * 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $models = \App\Model\Admin\Employee::with('company')->paginate(10);

        return view('admin.employee.list', ['data' => $models]);
    }

    function fetch_data(Request $request) {
        if ($request->ajax()) {


            $data = \App\Model\Admin\Employee::with('company')->paginate(10);

            return view('admin.employee.paginate_data', ['data' => $data])->render();
        }
    }

    public function create($id = 0) {
        $company = \App\Model\Admin\Company::all();

        if ($id != 0) {
            $model = \App\Model\Admin\Employee::find($id);
            if ($model) {
                return view('admin.employee.form', ['data' => $model, 'company' => $company]);
            }
        }
        return view('admin.employee.form', ['company' => $company]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id = 0) {
        $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'company_id' => 'required',
            'phone' => 'required|min:8|max:13|unique:employees,phone,' . $id,
            'email' => 'required|email|unique:employees,email,' . $id,
        ]);
        if ($id != 0) {

            $model = \App\Model\Admin\Employee::find($id);

            $model->first_name = $request->first_name;
            $model->last_name = $request->last_name;
            $model->company_id = $request->company_id;
            $model->phone = $request->phone;
            $model->email = $request->email;

            if ($model->save()) {
                $request->session()->flash('message', 'Employee record has been Updated Successfully!!!');
            } else {
                $request->session()->flash('message', 'Sorry! Employee records not been Updated...please try again');
            }
            return redirect()->route('emp.list');
        }

        $model = new \App\Model\Admin\Employee;
        if ($model) {
            $model->first_name = $request->first_name;
            $model->last_name = $request->last_name;
            $model->company_id = $request->company_id;
            $model->phone = $request->phone;
            $model->email = $request->email;
        }
        if ($model->save()) {
            $request->session()->flash('message', 'Employee has been Added Successfully!!!');
            return redirect()->route('emp.list');
        } else {
            $request->session()->flash('message', 'Sorry! Employee has not been Created,pease try again...');
            return view('admin.employee.form', ['data' => $model]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, $id) {
        $model = \App\Model\Admin\Employee::find($id);
        if ($model) {

            if ($model->delete()) {
                $request->session()->flash('message', 'Employee has been deleted Successfully!!!');
            } else {
                $request->session()->flash('message', 'Sorry! Employee  has not been deleted,please try again... ');
            }
            return redirect()->route('emp.list');
        }
        $request->session()->flash('message', 'Sorry! Employee has not been deleted,please try again... ');
        return redirect()->route('emp.list');
    }

}
