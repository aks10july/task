<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Model\Admin\Company;
class Employee extends Model
{
    
    protected $fillable = ['first_name','last_name','email','phone','comapny_id'];
    
    public function company(){
       return $this->hasOne(Company::class,'id','company_id');
    }
}
