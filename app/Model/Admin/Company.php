<?php

namespace App\Model\Admin;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
   protected $fillable=['name','email','logo'];
}
