<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


Route::get('/', function () {
    return view('welcome');
});;







Route::get('/home', 'HomeController@index')->name('home');



Auth::routes();


Route::get('admin/login', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'Admin\LoginController@login')->name('admin.login');
Route::post('admin/logout', 'Admin\LoginController@logout')->name('admin.logout');


Route::prefix('admin')->middleware('auth:admin')->group(function() {

    Route::get('/dashboard', 'Admin\DashboardController@index')->name('admin.dashboard');

    Route::get('/employee/list', 'Admin\EmployeeController@index')->name('emp.list');
    
    Route::get('/employee/create/{id?}', 'Admin\EmployeeController@create')->name('emp.create');

    
    
    Route::post( '/company/form/{id?}', 'Admin\EmployeeController@store')->name('emp.store');

    Route::get('/company/list/fetch_data','Admin\EmployeeController@fetch_data');

    Route::get('/company/delete/{id}', 'Admin\EmployeeController@delete')->name('emp.delete');

});








