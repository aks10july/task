<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('saveCompanyDetails','webservices\webservicesController@saveCompanyDetails');
Route::post('updateCompanyDetails','webservices\webservicesController@updateCompanyDetails');
Route::get('getCompanyDetailsById','webservices\webservicesController@getCompanyDetailsById');
Route::get('deleteCompanyById','webservices\webservicesController@deleteCompanyById');


