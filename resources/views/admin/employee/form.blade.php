@extends('admin.layouts.layout_1')
@section('page_specific_css_library')
<link rel="stylesheet" href="{{asset('assets/admin_assets/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/admin_assets/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('assets/admin_assets/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css')}}">
@stop
@section('page_specific_css')
<style>
    .logo-view-placeholder{
        width:100px !important;
    }
</style>
@stop
@section('page_content')

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Employee Form</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="{{(isset($data['id'])) ? route('emp.store',[$data['id']]) : route('emp.store')}}" method="POST" id="form">
        @csrf
        <div class="card-body">
          
                <div class="form-group col-md-6">
                    <label>Select Comapny<span class="text-danger">*</span></label>
                    <select class="form-control select2" name="company_id" style="width: 100%;" required>
                        <option value="">Select Company</option>
                        @if(isset($company))
                        @foreach($company as  $row)
                        @if(old('company_id', (isset($data->company_id) ? $data->company_id : '')) == $row->id)
                        <option value="{{$row->id}}" selected="selected">{{$row->name}}</option>
                        @else
                        <option  value="{{$row->id}}">{{$row->name}}</option>
                        @endif
                        @endforeach
                        @endif
                    </select>
                </div>
  

            

            <div class="row">
                <div class="form-group col-md-6">
                    <label for="first_name">First Name<span class="text-danger">*</span></label>
                    <input type="input" class="@error('first_name') is-invalid @enderror form-control" id="first_name" placeholder="Enter First Name " name="first_name" value="{{old('first_name',(isset($data)) ? $data['first_name'] : '')}}">
                    @error('first_name')
                    <span class=" text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="last_name">Last Name<span class="text-danger">*</span></label>
                    <input type="input" class="@error('last_name') is-invalid @enderror form-control" id="last_name" placeholder="Enter Last Name " name="last_name" value="{{old('last_name',(isset($data)) ? $data['last_name'] : '')}}">
                    @error('last_name')
                    <span class=" text-danger">{{ $message }}</span>
                    @enderror
                </div>
               
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="email">Email<span class="text-danger">*</span></label>
                    <input type="input" class="@error('email') is-invalid @enderror form-control" id="email" placeholder="Your Email " name="email" value="{{old('email',(isset($data)) ? $data['email'] : '')}}">
                    @error('email')
                    <span class=" text-danger">{{ $message }}</span>
                    @enderror
                </div>
                <div class="form-group col-md-6">
                    <label for="phone">Phone<span class="text-danger">*</span></label>
                    <input type="input" class="@error('phone') is-invalid @enderror form-control" id="phone" placeholder="Your Phone Number " name="phone" value="{{old('phone',(isset($data)) ? $data['phone'] : '')}}">
                    @error('phone')
                    <span class=" text-danger">{{ $message }}</span>
                    @enderror
                </div>
               
            </div>
            
        <!-- /.card-body -->

        <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <a href="{{route('emp.list')}}" class="btn btn-info">Back To List</a>
                <button class="btn btn-primary" type="reset">Reset</button>
                <button type="submit" class="btn btn-success">Submit</button>
            </div>
        </div>
    </form>
</div>


@stop
@section('page_specific_js_library')
<script src="{{asset('assets/admin_assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
<script src="{{asset('assets/admin_assets/plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('assets/admin_assets/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js')}}"></script>

@stop
@section('page_specific_js')
<script type="text/javascript">
                        $(document).ready(function () {
                            bsCustomFileInput.init();


                            //Initialize Select2 Elements
                            $('.select2bs4').select2({
                                theme: 'bootstrap4'
                            })
                            $('.duallistbox').bootstrapDualListbox()
                        });
                        $(document).ready(function(){
                            $("#form").validate({
                                rules:{
                                    'company_id'=>'required',
                                }
                            });
                        });




                       
</script>
@stop
