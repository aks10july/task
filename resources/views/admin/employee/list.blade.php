@extends('admin.layouts.layout_1')

@section('page_content')

    @if(Session::has('message'))
    <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h5> {{ Session::get('message')}}</h5>
               
    </div>
   
    @endif

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-md-9">
                <h3 class="card-title">Employee List</h3>
            </div>
           
  
             </div>
          <div class="row">
            <div class="col-md-3">
                <a class="btn btn-sm btn-success" href="{{route('emp.create')}}"><i class="fa fa-plus"></i> Add New Employee</a>
            </div>
          </div>

    </div>
    <!-- /.card-header -->
    <div class="card-body" id="table_data">
       @include('admin.employee.paginate_data')


    </div>
    <!-- /.card-body -->
    
</div>
 
@stop

@section('page_specific_js')
<script>
$(document).ready(function(){

$(document).on('click','.pagination a', function(e){
  event.preventDefault(e); 
  var page = $(this).attr('href').split('page=')[1];
  
 
  fetch_data(page);
});
 function fetch_data(page)
 {
  $.ajax({
   url:"{{url('admin/company/list/fetch_data?page=')}}" + page,
   success:function(data)
   {
       
    $('#table_data').html(data);
   }
  });
 }
 


});
</script>

@stop
