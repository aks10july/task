<table class="table table-bordered" >
    <thead>                  
        <tr>
            <th >Id</th>
            <th>Company</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th >Action</th>
        </tr>
    </thead>
    <tbody >

        @foreach($data as $row)
        <tr>
            <td>{{$row->id}}</td>
            <td>{{$row->company->name ?? ''}}</td>
            <td>{{$row->first_name}}</td>
            <td>{{$row->last_name}}</td>
            <td>{{$row->email}}</td>
            <td>{{$row->phone}}</td>

            <td>
                <div class="row">

                    <a href="{{route('emp.create',['id'=>$row->id])}}" class="btn-xs btn-warning m-lg-1">
                        <i class="fas fa-edit"></i> Edit
                    </a>
                    <a href="{{route('emp.delete',['id'=>$row->id])}}" class="btn-xs btn-danger m-lg-1">
                        <i class="fas fa-trash"></i> Delete
                    </a>


                </div>
            </td>
        </tr>
        @endforeach



    </tbody>


</table>
{{ $data->links() }}