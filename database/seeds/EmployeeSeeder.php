<?php

use Illuminate\Database\Seeder;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            for($i=0;$i<10;$i++){
        DB::table('employees')->insert([
            'first_name' => Str::random(7),
            'last_name' => Str::random(5),
         
            'email' => Str::random(10).'@gmail.com',
             'phone'=>rand(1000000000,9999999999),
            'company_id'=>rand(1,20),
            
        
        ]);
        }
    }
}
