<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=0;$i<10;$i++){
        DB::table('companies')->insert([
            'name' => Str::random(10),
         
            'email' => Str::random(10).'@gmail.com',
           
            'logo' => Str::random(5),
        
        ]);
        }
    }
}
